﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AiPaddleMove : MonoBehaviour {
	public Transform puck; //creates an object for the puck and defending goal
	public Transform goal;
	public float speed = 1; //constant speed for the paddle
	private Rigidbody rigidbody; //creates rigidbody object
	public Vector3 direction= new Vector3(0,0,0);

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		GetComponent<Rigidbody>().useGravity = false;
	}

	void FixedUpdate () {
		// get the input vales
		direction = (goal.position + puck.position) /2 - transform.position; //finds the midpoint between the puck and the goal, and tells the paddle to go to that direction with transform.position
		rigidbody.velocity = direction * speed; //sets the velocity
	}
}
