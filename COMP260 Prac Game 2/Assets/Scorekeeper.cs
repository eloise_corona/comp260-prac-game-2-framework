﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //adds the ui class

public class Scorekeeper : MonoBehaviour {
	static private Scorekeeper instance; //creates a single instance variable for Scorekeeper

	static public Scorekeeper Instance { //read-only access for instance. Can be called by other functions but cannot be altered
		get { return instance; }
	}

	public int pointsPerGoal = 1; //how much the points increments by for each goal
	private int[] score = new int[2]; //keeps the pair of scores in array
	public Text[] scoreText; //refers to the texts in the ui

	public void OnScoreGoal(int player){ //increases the score of the player by pointsPerGoal
		score [player] += pointsPerGoal;
		scoreText [player].text = score [player].ToString ();

		if(score[player] == 10){ //if any of the player's scores reaches 10
			scoreText [2].text = "Player" + player + " wins!"; //displays "player x wins!"
			Time.timeScale = 0; //stops the time in the game
		}
	}

	// Use this for initialization
	void Start () {
		if (instance == null) {
			instance = this; //save this instance
		} else {
			Debug.LogError ("More than one Scorekeeper exists in the scene."); //if more than one instance exists
		}

		for (int i = 0; i < score.Length; i++) { //resets score to 0
			score [i] = 9;
			scoreText [i].text = "9";
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
}
