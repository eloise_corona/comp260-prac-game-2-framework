﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip; //audio clip for wall collision
	public AudioClip paddleCollideClip; //audio for paddle collision
	public LayerMask paddleLayer; //the paddle layer
	private Rigidbody rigidbody;
	private AudioSource audio;
	public Transform startingPos;

	void Start () {
		audio = GetComponent<AudioSource> (); //gets the audio file and stores it in "audio", making it easier to change audio files
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();
	}

	public void ResetPosition() {
		rigidbody.MovePosition(startingPos.position); //moves the puck to the starting position
		rigidbody.velocity = Vector3.zero; //resets the velocity of the puck
	}

	void OnCollisionEnter(Collision collision) { //when the puck collides, play audio
		if (paddleLayer.Contains(collision.gameObject)) { //if puck hits the paddle, play paddleCollideClip
			audio.PlayOneShot(paddleCollideClip);
		}
		else { //else play wallCollideClip
			audio.PlayOneShot(wallCollideClip);
		}

	}
}
