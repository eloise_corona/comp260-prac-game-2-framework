﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;
	public float speed = 20f;
	public float force = 10f;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;


	}

	// Update is called once per frame
	void Update () {
		Debug.Log("Time = " + Time.time);
		Debug.Log(Time.deltaTime);
	}




	void FixedUpdate () {
		// get the input values
		Vector3 direction = new Vector3(0,0,0);
		direction.x = Input.GetAxis("Horizontal");
		direction.z = Input.GetAxis("Vertical");

		// scale by the maxSpeed parameter
		//Vector3 velocity = direction * maxSpeed;

		// move the object
		//transform.Translate(velocity * Time.deltaTime);

		//Vector3 pos = GetMousePosition();
		//Vector3 dir = pos - rigidbody.position;
		//dir = dir.normalized;
		rigidbody.velocity = direction * speed;

		Debug.Log("Fixed Time = " + Time.fixedTime);

		//rigidbody.AddForce(dir.normalized * force);
	}

	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

	// Use this for initialization


}
