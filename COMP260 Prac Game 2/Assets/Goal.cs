﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

	public AudioClip scoreClip; //goal clip
	private AudioSource audio;
	public int player = 1; //who gets points for scoring in this goal

	void Start() {
		audio = GetComponent<AudioSource> (); //constructor for audio
	}

	void OnTriggerEnter(Collider collider) { //when the collison happens, play scoreClip
		// play score sound
		audio.PlayOneShot(scoreClip);

		Scorekeeper.Instance.OnScoreGoal (player);

		PuckControl puck = collider.gameObject.GetComponent<PuckControl>(); //gets reference to PuckControl
		puck.ResetPosition(); //calls public class to reset position of puck

	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
